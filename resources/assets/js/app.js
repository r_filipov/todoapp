

require('./bootstrap');
window.Vue = require('vue');


import Controllers from './controllers/index';

document.addEventListener('DOMContentLoaded', function () {

    class Application {
        constructor() {

            const [controller, action] = $('body').attr('data-page').split(":").slice(-2);

            const controllerObject = Controllers[controller] ? new Controllers[controller]() : ''

            if (Object.prototype.toString.call(controllerObject) === "[object Object]") {

                if (typeof controllerObject[action] === 'function') {
                    controllerObject[action]();
                }
            }

        }
    }

    new Application();
});

